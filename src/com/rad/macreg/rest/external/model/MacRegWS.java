package com.rad.macreg.rest.external.model;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.representation.Form;

@Path("/ws")
public class MacRegWS {

	//String WEBSERVICE_HOST  = "http://192.168.24.64:8080/";  // webservices01   //"http://localhost:8080//"; //
	String WEBSERVICE_HOST  = "http://192.168.24.66:8080/";  // webservices03  Production
	//String WEBSERVICE_HOST  = "http://localhost:8080/";  // webservices03
	//String WEBSERVICE_HOST  = "http://172.17.130.65:8080/";
	String WEBSERVICE_PATH  = "MacRegREST/MacReg/Qry/";
	public static final String BUILD = "180822";



@POST
@Path("/q001_mac_extant_chack")
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
public  Response q001_mac_extant_chack(@FormParam("macID") String macID)   {
	
	Form f = new Form();    
	f.add("macID", macID);
		
	return getPostDataFromServer("q001_mac_extant_chack",  f);
	
}

@POST
@Path("/q003_idnumber_extant_check")
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
public  Response q003_idnumber_extant_check(@FormParam("idNumber") String idNumber)   {
	
	Form f = new Form();    
	f.add("idNumber", idNumber);
		
	return getPostDataFromServer("q003_idnumber_extant_check",  f);
	
}




@POST
@Path("/qry_check_valid_idnumber")
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
public  Response qry_check_valid_idnumber(@FormParam("idNumber") String idNumber)   {
	
	Form f = new Form();    
	f.add("idNumber", idNumber);
		
	return getPostDataFromServer("qry_check_valid_idnumber",  f);
	
}

@POST
@Path("/q002_delete_mac_extant")
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
public  Response q002_delete_mac_extant(@FormParam("macID") String macID, @FormParam("HAND")  String secret)   {
	
	Form f = new Form();    
	f.add("macID", macID);
	f.add("HAND", secret);
		
	return getPostDataFromServer("q002_delete_mac_extant",  f);

}

@POST
@Path("/qupdate")
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
public  Response qupdate(@FormParam("ID_NUMBER") String idNumber,
		                 @FormParam("MAC")      String mac,
		                 @FormParam("SP_TYPE1") String spType1,
		                 @FormParam("SP_TYPE2") String spType2,
		                 @FormParam("SP_TYPE3") String spType3,
		                 @FormParam("SP_TYPE4") String spType4,
		                 @FormParam("SP_TYPE5") String spType5,
		                 @FormParam("SP_TYPE6") String spType6,
		                 @FormParam("SP_TYPE7") String spType7,
		                 @FormParam("SP_TYPE8") String spType8,
		                 @FormParam("APP_VER")  String appVer,
		                 @FormParam("IMEI")  String imei,
		                 @FormParam("IMEI2")    String imei2,
		                 @FormParam("HAND")  String secret)   {
	
	Form f = new Form();    
	f.add("ID_NUMBER", idNumber);
	f.add("MAC", mac);
	f.add("SP_TYPE1", spType1);
	f.add("SP_TYPE2", spType2);
	f.add("SP_TYPE3", spType3);
	f.add("SP_TYPE4", spType4);
	f.add("SP_TYPE5", spType5);
	f.add("SP_TYPE6", spType6);
	f.add("SP_TYPE7", spType7);
	f.add("SP_TYPE8", spType8);
	f.add("APP_VER", appVer);
	f.add("IMEI", imei);
	f.add("IMEI2", imei2);
	f.add("HAND", secret);
	
	return getPostDataFromServer("qupdate",  f);


}


@POST
@Path("/qupdateSeq")
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
public  Response qupdateSeq(@FormParam("ID_NUMBER") String idNumber,
		                 @FormParam("MAC")      String mac,
		                 @FormParam("SP_TYPE1") String spType1,
		                 @FormParam("SP_TYPE2") String spType2,
		                 @FormParam("SP_TYPE3") String spType3,
		                 @FormParam("SP_TYPE4") String spType4,
		                 @FormParam("SP_TYPE5") String spType5,
		                 @FormParam("SP_TYPE6") String spType6,
		                 @FormParam("SP_TYPE7") String spType7,
		                 @FormParam("SP_TYPE8") String spType8,
		                 @FormParam("APP_VER")  String appVer,
		                 @FormParam("SEQ")  String seq,
		                 @FormParam("IMEI")  String imei,
		                 @FormParam("IMEI2")    String imei2,
		                 @FormParam("HAND")  String secret)   {
	
	Form f = new Form();    
	f.add("ID_NUMBER", idNumber);
	f.add("MAC", mac);
	f.add("SP_TYPE1", spType1);
	f.add("SP_TYPE2", spType2);
	f.add("SP_TYPE3", spType3);
	f.add("SP_TYPE4", spType4);
	f.add("SP_TYPE5", spType5);
	f.add("SP_TYPE6", spType6);
	f.add("SP_TYPE7", spType7);
	f.add("SP_TYPE8", spType8);
	f.add("APP_VER", appVer);
	f.add("SEQ", seq);
	f.add("IMEI", imei);
	f.add("IMEI2", imei2);
	f.add("HAND", secret);
	
	return getPostDataFromServer("qupdate",  f);

}

private Response getDataFromServer(String param){
	
    String webserviceURL = WEBSERVICE_HOST+WEBSERVICE_PATH+param;
	
	Client client = Client.create();

	WebResource webResource = client
	   .resource(webserviceURL); 

	ClientResponse response = webResource.type("application/json")
	   .get(ClientResponse.class);
	
	
	String output = response.getEntity(String.class);
	return  Response.ok(output).build();
}


private Response getPostDataFromServer(String param, Form f){
	
    String webserviceURL = WEBSERVICE_HOST+WEBSERVICE_PATH;
	
 System.out.println("webserviceURL="+webserviceURL);	
    ClientConfig config = new DefaultClientConfig();
    Client client = Client.create(config);
    WebResource service = client.resource(UriBuilder.fromUri(webserviceURL).build());

    String output = service.path(param).accept(MediaType.APPLICATION_JSON).post(String.class,f);
 System.out.println("output="+output);
	
	return  Response.ok(output).build();
}


@GET
@Path("/Ping")
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
public Response getPing(@QueryParam("params") String params) {

	//System.out.println("getPing  Build 26 06 2017");
	
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	Calendar cal = Calendar.getInstance();
	//System.out.println("1  "+dateFormat.format(cal.getTime())); 
	String hostname = null;
	try {
		
		InetAddress  ip = InetAddress.getLocalHost();
		hostname = ip.getHostName();
				
	} catch (UnknownHostException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	//String[] paramList = { params};
	JsonObjectBuilder jpb = Json.createObjectBuilder();
	JsonArrayBuilder jb = Json.createArrayBuilder();
	JsonObjectBuilder jsonResult = Json.createObjectBuilder();
	Gson gson = new GsonBuilder().setPrettyPrinting().create();
	
	try {

		
		jpb = Json.createObjectBuilder();
		jpb.add("ping result", "ping + | date= "+dateFormat.format(cal.getTime())+"  build="+BUILD +" ~~~ Server ~~~  = "+hostname );
		jb.add(jpb);
		jsonResult.add("Lists", jb);
	}
	 finally {
		

	}
	
	String json = jsonResult.build().toString();

	JsonParser jp = new JsonParser();
	JsonElement je = jp.parse(json);
	String prettyJsonString = gson.toJson(je);

	if (jb == null) {

		return Response.status(Response.Status.BAD_REQUEST).build();

	} else {

		return Response.ok(prettyJsonString).build();

	}
}


@GET
@Path("/ServerPing")
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
public Response getServerPing() {

	String param = "Ping";
	return getDataFromServer(param);

	
}


@POST
@Path("/check_imei_valid")
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
public Response  validateIMEINumber(@FormParam("imei") String imei) {
	
	Form f = new Form();    
	f.add("imei", imei);
			
	return getPostDataFromServer("check_imei_valid",  f);
	
}

@POST
@Path("/q004_imei_check")
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
public  Response q004_imei_check(@FormParam("mac") String mac, @FormParam("idnumber") String idnumber)   {
	
	Form f = new Form();    
	f.add("mac", mac);
	f.add("idnumber", idnumber);
		
	return getPostDataFromServer("q004_imei_check",  f);
	
}




@POST
@Path("/q005_imei_check")
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
public  Response q005_imei_check(@FormParam("imei") String imei)   {
	
	Form f = new Form();    
	f.add("imei", imei);
			
	return getPostDataFromServer("q005_imei_check",  f);
}

@POST
@Path("/q006_imei_delete")
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
public  Response q006_imei_delete(@FormParam("idnumber") String idnumber, @FormParam("HAND")  String secret)   {
	
	Form f = new Form();    
	f.add("idnumber", idnumber);
	f.add("HAND", secret);
			
	return getPostDataFromServer("q006_imei_delete",  f);
}


@POST
@Path("/q0041_imei_check")
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
public  Response q0041_imei_check(@FormParam("mac") String mac, @FormParam("idnumber") String idnumber, @FormParam("IMEI_INDEX")  String imeiIndex)   {
	
	Form f = new Form();    
	f.add("mac", mac);
	f.add("idnumber", idnumber);
	f.add("imeiIndex", imeiIndex);
		
	return getPostDataFromServer("q0041_imei_check",  f);
	
}
@POST
@Path("/q0051_imei_check")
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
public  Response q0051_imei_check(@FormParam("imei") String imei, @FormParam("IMEI_INDEX")  String imeiIndex)   {
	
	Form f = new Form();    
	f.add("imei", imei);
	f.add("imeiIndex", imeiIndex);
			
	return getPostDataFromServer("q0051_imei_check",  f);
}

@POST
@Path("/q0061_imei_delete")
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
public  Response q0061_imei_delete(@FormParam("idnumber") String idnumber, @FormParam("HAND")  String secret, @FormParam("IMEI_INDEX")  String imeiIndex)   {
	
	Form f = new Form();    
	f.add("idnumber", idnumber);
	f.add("HAND", secret);
	f.add("imeiIndex", imeiIndex);
			
	return getPostDataFromServer("q0061_imei_delete",  f);
}

/*@POST
@Path("/q001_get_current_mac")
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
public  Response q001_get_current_mac()   {
	
	Form f = new Form();    
				
	return getPostDataFromServer("q001_get_current_mac",  f);
}*/

@POST
@Path("/q002_get_idnumber_data")
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
public  Response q002_get_idnumber_data(@FormParam("idnumber") String idnumber)   {
	
	Form f = new Form();    
	f.add("idnumber", idnumber);
	
	return getPostDataFromServer("q002_get_idnumber_data",  f);
}

@POST
@Path("/sp001_mac_address_alloc")
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
public  Response sp001_mac_address_alloc(@FormParam("p_mode") String p_mode,
											@FormParam("p_trace_id") String p_trace_id,
											@FormParam("p_serial") String p_serial,
											@FormParam("p_idnumber_id") String p_idnumber_id,
											@FormParam("p_alloc_qty") String p_alloc_qty,
											@FormParam("p_file_version") String p_file_version
											
											)   {
	
	Form f = new Form();
	f.add("p_mode", p_mode);
	f.add("p_trace_id", p_trace_id);
	f.add("p_serial", p_serial);
	f.add("p_idnumber_id", p_idnumber_id);
	f.add("p_alloc_qty", p_alloc_qty);
	f.add("p_file_version", p_file_version);
	
	
	
	return getPostDataFromServer("sp001_mac_address_alloc",  f);
}

@POST
@Path("/getConfirmationMessage")
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
public  Response getConfirmationMessage(@FormParam("idnumber") String idnumber,@FormParam("mac1") String mac1,@FormParam("mac2") String mac2,@FormParam("imei1") String imei1,@FormParam("imei2") String imei2){

	Form f = new Form();
	f.add("idnumber", idnumber);
	f.add("mac1", mac1);
	f.add("mac2", mac2);
	f.add("imei1", imei1);
	f.add("imei2", imei2);
	
	return getPostDataFromServer("getConfirmationMessage",  f);
}
}

package com.rad.macreg.rest.internal.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.rad.macreg.rest.dao.implementation.UtilsDAO;
import com.rad.macreg.rest.errors.ConnectionException;
import com.rad.macreg.rest.model.IConsts;
import com.rad.macreg.rest.model.ModelUtils;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

@Path("/Qry")
public class MacRegREST extends UtilsDAO implements IConsts {

	List<String> dbSchemes = Arrays.asList("inventory");
	public static final String BUILD = "040922";

	static Logger logger = LogManager.getLogger(MacRegREST.class);
	public static boolean appDebug = false;
	final static String plaintext = "MACREG@RAD_WS";
	// http://stackoverflow.com/questions/8194408/how-to-access-parameters-in-a-restful-post-method

	public static void main(String[] args) {

		MacRegREST mrr = new MacRegREST();
		// mrr.getConfirmationMessage("EA100463652", "123456789014",
		// "AABBCCDDEEF1", null, null);
		// mrr.getConfirmationMessage("EA100463652", "0020D2D4262A",
		// "0020D2EBBFDA", null, null);
		///123456789013 /AABBCCDDEEF1   /EA100463652 /DISABLE /DISABLE /DISABLE /DISABLE /DISABLE /DISABLE /DISABLE /DISABLE
		appDebug = true;
		//mrr.getConfirmationMessage("EA100463652", "123456789014", "0020D2EBBFDA", null, null);
		//mrr.getConfirmationMessage("EA100463652", "123456789013", "AABBCCDDEEF1", null, null);
		//mrr.getConfirmationMessage("EA100463652", "123456789013", "", "1234567890123", null);
		mrr.q001_mac_extant_chack("0020D2606CBF");
	}

	@POST
	@Path("/q001_mac_extant_chack")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
	public Response q001_mac_extant_chack(@FormParam("macID") String macID) {

		logger.debug("q001_mac_extant_chack macID="+macID);
		String sql = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "qry.q001.mac.extant.chack");
		String[] paramList = { macID };
		return createJSONResponse(paramList, "inventory", sql, "q001");

	}

	@POST
	@Path("/q003_idnumber_extant_check")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
	public Response q003_idnumber_extant_check(@FormParam("idNumber") String idNumber) {

		logger.debug("q003_idnumber_extant_check idNumber="+idNumber);
		String sql = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "q003_idnumber_extant_check");
		String[] paramList = { idNumber };
		return createJSONResponse(paramList, "inventory", sql, "q003");

	}

	@POST
	@Path("/qry_check_valid_idnumber")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
	public Response qry_check_valid_idnumber(@FormParam("idNumber") String idNumber) {

		logger.debug("qry_check_valid_idnumber idNumber="+idNumber);
		String sql = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "qry.check.valid.idnumber");
		String[] paramList = { idNumber };
		//Connection connPool = null;
		String result = "";
		String stringValue = null;
		int itemsCounter = 0;

		try {

			String datasource = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "inventory");
			conn = super.getConnection(datasource);

			if (paramList.length == 1 && paramList[0].isEmpty()) {
				logger.debug("2 paramList Is empty");
				logger.debug("paramList[0]"+paramList[0]);
				paramList = null;
			}
logger.debug("conn= "+conn+" qry_check_valid_idnumber");

           if(conn == null) {
        	   
        	   logger.debug("conn= "+conn+" qry_check_valid_idnumber 2");
        	   return Response.status(Response.Status.BAD_REQUEST).build();
           }
           
			ResultSet rs = super.getQueryResult(conn, sql, paramList);
			if (rs == null) {

				System.out.println("rs == null" + "  Conn=" + conn + "  sql=" + sql);
				result = "false";
			} else
				result = "true";

		} catch (ConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} finally {
			if (conn != null)
				super.closeConnection(conn);

		}
		String output = "{\"Name\":\"qry.check.valid.idnumber\",\"result\":" + result + "}";
		//System.out.println(output);
		return Response.ok(output).build();

	}

	@POST
	@Path("/q002_delete_mac_extant")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
	public Response q002_delete_mac_extant(@FormParam("macID") String macID, @FormParam("HAND") String secret) {

		logger.debug("q002_delete_mac_extant macID="+macID);
		int result = -100;
		// System.out.println("q002_delete_mac_extant build 31-12-17");
		String sql = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "q002.delete.mac.extant");
		// String[] paramList = { macID };

		String md5text = DigestUtils.md5Hex(plaintext + macID);
		// System.out.println("md5text= "+ md5text );
		// System.out.println("secret= "+ secret );

		if (!secret.equals(md5text)) {

			String output = "{\"Name\":\"qupdate\",\"result\":" + result + "}";
			logger.debug("q002_delete_mac_extant -- FAILED -- macID=" + macID + "  secret=" + secret);
			return Response.ok(output).build();
		}

		try {
			String datasource = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "inventory");
			conn = super.getConnection(datasource);
			
			if(conn == null) {
		      	   
		      	   logger.debug("conn= "+conn+" q002_delete_mac_extant");
		      	   
		      	   return Response.status(Response.Status.BAD_REQUEST).build();
		         }
			
			conn.setAutoCommit(true);
			
			CallableStatement update = conn.prepareCall(sql);
			update.setString(1, macID); /* wor_d_orders.id orderDetailID */

			result = update.executeUpdate();

		} catch (ConnectionException | SQLException e) {

			e.printStackTrace();
			String output = "{\"Name\":\"q002_delete_mac_extant\",\"result\":" + result + "}";
			logger.debug("q002_delete_mac_extant-- FAILED --- macID=" + macID + "  secret=" + secret);
			return Response.ok(output).build();

		} finally {
			if (conn != null)
				super.closeConnection(conn);
		}

		String output = "{\"Name\":\"q002_delete_mac_extant\",\"result\":" + result + "}";
		logger.debug("q002_delete_mac_extant macID=" + macID + "  secret=" + secret);
		return Response.ok(output).build();

	}

	@POST
	@Path("/qupdate")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
	public Response qupdate(@FormParam("ID_NUMBER") String idNumber, @FormParam("MAC") String mac,
			@FormParam("SP_TYPE1") String spType1, @FormParam("SP_TYPE2") String spType2,
			@FormParam("SP_TYPE3") String spType3, @FormParam("SP_TYPE4") String spType4,
			@FormParam("SP_TYPE5") String spType5, @FormParam("SP_TYPE6") String spType6,
			@FormParam("SP_TYPE7") String spType7, @FormParam("SP_TYPE8") String spType8,
			@FormParam("APP_VER") String appVer, @FormParam("IMEI") String imei, @FormParam("IMEI2") String imei2,
			@FormParam("HAND") String secret) {

		int result = -100;
		logger.debug("qupdate idNumber="+idNumber+" mac="+mac+" imei="+imei+" imei2="+imei2+" spType1="+spType1+" spType2="+spType2+" spType3="+spType3+" spType4="+spType4+" spType5="+spType5+" spType6="+spType6+" spType7="+spType7+" spType8="+spType8);
		String sql = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "qupdate");

		String md5text = DigestUtils.md5Hex(plaintext + mac);
		// System.out.println("md5text= "+ md5text );
		// System.out.println("secret= "+ secret );

		System.out.println("mac= " + mac);
		System.out.println("idNumber= " + idNumber);
		System.out.println("spType1= " + spType1);
		System.out.println("spType2= " + spType2);
		System.out.println("spType3= " + spType3);
		System.out.println("spType4= " + spType4);
		System.out.println("spType5= " + spType5);
		System.out.println("spType6= " + spType5);

		if (!secret.equals(md5text)) {

			String output = "{\"Name\":\"qupdate\",\"result\":" + result + "}";
			logger.debug("secret not equal qupdate FAILED macID=" + mac + " idNumber=" + idNumber + "  secret=" + secret
					+ "  md5text= " + md5text);
			return Response.ok(output).build();
		}

		try {
			String datasource = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "inventory");
			conn = super.getConnection(datasource);
			if(conn == null) {
		      	   
		      	   logger.debug("conn= "+conn+" qupdate");
		      	   
		      	   return Response.status(Response.Status.BAD_REQUEST).build();
		         }
			conn.setAutoCommit(true);

			CallableStatement update = conn.prepareCall(sql);

			update.setString(1, idNumber);
			update.setString(2, mac);
			update.setString(3, spType1);
			update.setString(4, spType2);
			update.setString(5, spType3);
			update.setString(6, spType4);
			update.setString(7, spType5);
			update.setString(8, spType6);
			update.setString(9, spType7);
			update.setString(10, spType8);
			update.setString(11, appVer);
			update.setString(12, imei);
			update.setString(13, imei2);

			result = update.executeUpdate();

			logger.debug("qupdate idNumber=" + idNumber + " mac= " + mac + " imei= " + imei + " imei2= " + imei2
					+ " result=" + result);

		} catch (ConnectionException | SQLException e) {

			e.printStackTrace();
			String output = "{\"Name\":\"qupdate\",\"result\":" + result + "}";
			logger.debug("qupdate FAILED macID=" + mac + " idNumber=" + idNumber + "  secret=" + secret);
			return Response.ok(output).build();

		} finally {
			if (conn != null)
				super.closeConnection(conn);
		}

		String output = "{\"Name\":\"qupdate\",\"result\":" + result + "}";
		logger.debug("qupdate macID=" + mac + " idNumber=" + idNumber + "  secret=" + secret);
		return Response.ok(output).build();

	}

	@POST
	@Path("/qupdateSeq")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
	public Response qupdateSeq(@FormParam("ID_NUMBER") String idNumber, @FormParam("MAC") String mac,
			@FormParam("SP_TYPE1") String spType1, @FormParam("SP_TYPE2") String spType2,
			@FormParam("SP_TYPE3") String spType3, @FormParam("SP_TYPE4") String spType4,
			@FormParam("SP_TYPE5") String spType5, @FormParam("SP_TYPE6") String spType6,
			@FormParam("SP_TYPE7") String spType7, @FormParam("SP_TYPE8") String spType8,
			@FormParam("APP_VER") String appVer, @FormParam("SEQ") String seq, @FormParam("IMEI") String imei,
			@FormParam("IMEI2") String imei2, @FormParam("HAND") String secret) {

		int result = -100;
		// System.out.println(" build 2");
		logger.debug("qupdateSeq idNumber="+idNumber+" mac="+mac+" imei="+imei+" imei2="+imei2+" seq="+seq+" spType1="+spType1+" spType2="+spType2+" spType3="+spType3+" spType4="+spType4+" spType5="+spType5+" spType6="+spType6+" spType7="+spType7+" spType8="+spType8);
		
		String sql = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "qupdate2");
		// String[] paramList = { idNumber, mac, spType1, spType2, spType3,
		// spType4, spType5, spType6, spType7, spType8, appVer};

		String md5text = DigestUtils.md5Hex(plaintext + mac);

		if (!secret.equals(md5text)) {

			String output = "{\"Name\":\"qupdate\",\"result\":" + result + "}";
			logger.debug(
					"qupdateSeq WRONG secret FAILED macID=" + mac + " idNumber=" + idNumber + "  secret=" + secret);
			return Response.ok(output).build();
		}

		try {
			String datasource = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "inventory");
			conn = super.getConnection(datasource);

			  if(conn == null) {
				   
				   
	        	   logger.debug("conn= "+conn+" qupdateSeq");
	        	   return Response.status(Response.Status.BAD_REQUEST).build();
	           }
			conn.setAutoCommit(true);

			CallableStatement update = conn.prepareCall(sql);

			update.setString(1, idNumber);
			update.setString(2, mac);
			update.setString(3, spType1);
			update.setString(4, spType2);
			update.setString(5, spType3);
			update.setString(6, spType4);
			update.setString(7, spType5);
			update.setString(8, spType6);
			update.setString(9, spType7);
			update.setString(10, spType8);
			update.setString(11, appVer);
			update.setString(12, seq);
			update.setString(13, imei);
			update.setString(14, imei2);

			result = update.executeUpdate();

			logger.debug("qupdateSeq idNumber=" + idNumber + " mac= " + mac + " imei= " + imei + " imei2= " + imei2
					+ " seq= " + seq + " result=" + result);

		} catch (ConnectionException | SQLException e) {

			e.printStackTrace();
			String output = "{\"Name\":\"qupdate2\",\"result\":" + result + "}";
			logger.debug("qupdateSeq FAILED macID=" + mac + " idNumber=" + idNumber + "  secret=" + secret);
			logger.debug("Error message= " + e.getMessage());
			return Response.ok(output).build();

		} finally {
			if (conn != null)
				super.closeConnection(conn);
		}

		String output = "{\"Name\":\"qupdate2\",\"result\":" + result + "}";
		logger.debug("qupdateSeq   macID=" + mac + " idNumber=" + idNumber + "  secret=" + secret);
		return Response.ok(output).build();

	}

	/**
	 * 
	 * q004_imei_check will check the following : MAC and id number are the same
	 * in DB but imei is different ( is a new number or blank ) Will have a
	 * result only when deleting OR replacing current imei - happens if new imei
	 * is null or a new number . Otherwise is empty.
	 * 
	 * @param mac
	 * @param idnumber
	 * @return
	 */
	@POST
	@Path("/q004_imei_check")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
	public Response q004_imei_check(@FormParam("mac") String mac, @FormParam("idnumber") String idnumber) {

		String sql = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "q004_imei_check");

		logger.debug("q004_imei_check mac="+mac+" idnumber="+idnumber);
		String[] paramList = { mac.trim(), idnumber.trim() }; // , imei };
		return createJSONResponse(paramList, "inventory", sql, "q004");

	}

	/**
	 * 
	 * q005_imei_check will check the following : If current imei - is already
	 * used with another ID. if imei is null then (Length(imei) <= 0
	 * 
	 * 
	 * 
	 * @param imei
	 * @return
	 */

	@POST
	@Path("/q005_imei_check")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
	public Response q005_imei_check(@FormParam("imei") String imei) {

		String sql = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "q005_imei_check");
		logger.debug("q005_imei_check imei="+imei);
		if (imei == null || imei.length() < 1) {
			imei = " ";

			// String output = "{\"result\":}";
			return Response.ok().build();

		}
		String[] paramList = { imei };
		return createJSONResponse(paramList, "inventory", sql, "q005");

	}

	@POST
	@Path("/q006_imei_delete")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
	public Response q006_imei_delete(@FormParam("idnumber") String idnumber, @FormParam("HAND") String secret) {

		int result = -100;
		// System.out.println("2 q006_imei_delete idnumber="+idnumber);
		String sql = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "q006_imei_delete");
		// String[] paramList = { macID };
		logger.debug("q006_imei_delete idnumber="+idnumber);
		String md5text = DigestUtils.md5Hex(plaintext + idnumber);
		// System.out.println("md5text= "+ md5text );
		// System.out.println("secret= "+ secret );

		if (!secret.equals(md5text)) {

			String output = "{\"Name\":\"qupdate\",\"result\":" + result + "}";
			logger.debug("q006_imei_delete output=" + output + "  idnumber=" + idnumber);
			return Response.ok(output).build();
		}

		try {
			String datasource = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "inventory");
			conn = super.getConnection(datasource);

			if(conn == null) {
				   
				   
	        	   logger.debug("conn= "+conn+" qupdateSeq");
	        	   return Response.status(Response.Status.BAD_REQUEST).build();
	           }
			conn.setAutoCommit(true);

			CallableStatement update = conn.prepareCall(sql);
			update.setString(1, idnumber);
			// update.setString(2,imei);

			result = update.executeUpdate();

		} catch (ConnectionException | SQLException e) {

			e.printStackTrace();
			String output = "{\"Name\":\"q006_imei_delete\",\"result\":" + result + "}";
			return Response.ok(output).build();

		} finally {
			if (conn != null)
				super.closeConnection(conn);
		}

		String output = "{\"Name\":\"q006_imei_delete\",\"result\":" + result + "}";
		logger.debug("q006_imei_delete=" + output);
		return Response.ok(output).build();

	}

	@POST
	@Path("/q0041_imei_check")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
	public Response q0041_imei_check(@FormParam("mac") String mac, @FormParam("idnumber") String idnumber,
			@FormParam("Imei_Index") String imeiIndex) {
		String sql;
		System.out.println("12  imeiIndex=" + imeiIndex);
		System.out.println("12  idnumber=" + idnumber);
		System.out.println("12  mac=" + mac);
		logger.debug("q0041_imei_check mac="+mac+" idnumber="+idnumber);
		if (imeiIndex != null && imeiIndex.equals("2"))
			sql = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "q0041_imei_check_imei2");
		else
			sql = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "q004_imei_check");
		// if ( imei == null || imei.length() < 1)
		// imei =" ";
		// System.out.println("mac="+mac);
		// System.out.println("idnumber="+idnumber);
		String[] paramList = { mac.trim(), idnumber.trim() }; // , imei };
		return createJSONResponse(paramList, "inventory", sql, "q0041");

	}

	@POST
	@Path("/q0051_imei_check")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
	public Response q0051_imei_check(@FormParam("imei") String imei, @FormParam("Imei_Index") String imeiIndex) {

		String sql; // = ModelUtils.getResource(IConsts.ORACLE_RESOURCES,
					// "q0051_imei_check_imei2");
		logger.debug("q0051_imei_check imei="+imei+" Imei_Index="+imeiIndex);
		if (imeiIndex != null && imeiIndex.equals("2"))
			sql = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "q0051_imei_check_imei2");
		else
			sql = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "q005_imei_check");

		if (imei == null || imei.length() < 1) {
			imei = " ";

			// String output = "{\"result\":}";
			return Response.ok().build();

		}
		String[] paramList = { imei };
		return createJSONResponse(paramList, "inventory", sql, "q0051");

	}

	@POST
	@Path("/q0061_imei_delete")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
	public Response q0061_imei_delete(@FormParam("idnumber") String idnumber, @FormParam("HAND") String secret,
			@FormParam("Imei_Index") String imeiIndex) {

		int result = -100;
		// System.out.println("2 q006_imei_delete idnumber="+idnumber);
		String sql; // = ModelUtils.getResource(IConsts.ORACLE_RESOURCES,
					// "q0061_imei_delete_imei2");
		// String[] paramList = { macID };
		logger.debug("q0061_imei_delete idnumber="+idnumber);
		if (imeiIndex != null && imeiIndex.equals("2"))
			sql = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "q0061_imei_delete_imei2");
		else
			sql = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "q006_imei_delete_imei");
		String md5text = DigestUtils.md5Hex(plaintext + idnumber);
		// System.out.println("md5text= "+ md5text );
		// System.out.println("secret= "+ secret );

		if (!secret.equals(md5text)) {

			String output = "{\"Name\":\"qupdate\",\"result\":" + result + "}";
			logger.debug("q0061_imei_delete output=" + output + "  idnumber=" + idnumber);
			return Response.ok(output).build();
		}

		try {
			String datasource = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "inventory");
			conn = super.getConnection(datasource);

			if(conn == null) {
				   
				   
	        	   logger.debug("conn= "+conn+" q0061_imei_delete");
	        	   return Response.status(Response.Status.BAD_REQUEST).build();
	           }
			
			conn.setAutoCommit(true);

			CallableStatement update = conn.prepareCall(sql);
			update.setString(1, idnumber);
			// update.setString(2,imei);

			result = update.executeUpdate();

		} catch (ConnectionException | SQLException e) {

			e.printStackTrace();
			String output = "{\"Name\":\"q0061_imei_delete\",\"result\":" + result + "}";
			return Response.ok(output).build();

		} finally {
			if (conn != null)
				super.closeConnection(conn);
		}

		String output = "{\"Name\":\"q0061_imei_delete\",\"result\":" + result + "}";
		logger.debug("q0061_imei_delete=" + output);
		return Response.ok(output).build();

	}

	@POST
	@Path("/q002_get_idnumber_data")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
	public Response q002_get_idnumber_data(@FormParam("idnumber") String idnumber) {
		String sql;

		logger.debug("q002_get_idnumber_data idnumber="+idnumber);
		sql = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "q002_get_idnumber_data");

		String[] paramList = { idnumber };
		return createJSONResponse(paramList, "inventory", sql, "q002_get_idnumber_data");

	}

	@POST
	@Path("/sp001_mac_address_alloc")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
	public Response sp001_mac_address_alloc(@FormParam("p_mode") String p_mode,
			@FormParam("p_trace_id") String p_trace_id, @FormParam("p_serial") String p_serial,
			@FormParam("p_idnumber_id") String p_idnumber_id, @FormParam("p_alloc_qty") String p_alloc_qty,
			@FormParam("p_file_version") String p_file_version) {
		String sql;
		String currentMACAddress;
		String newMACAddress;

		JsonObjectBuilder jpb = Json.createObjectBuilder();
		JsonArrayBuilder jb = Json.createArrayBuilder();
		JsonObjectBuilder jsonResult = Json.createObjectBuilder();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		try {

			String datasource = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "inventory");
			conn = super.getConnection(datasource);
			
			if(conn == null) {
				   
				   
	        	   logger.debug("conn= "+conn+" sp001_mac_address_alloc");
	        	   return Response.status(Response.Status.BAD_REQUEST).build();
	           }
			
			currentMACAddress = getCurrMACAddress(conn);
			newMACAddress = CalcNewMACAddress(currentMACAddress, Integer.valueOf(p_alloc_qty));
			CallableStatement storedProc = null;

			sql = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "sp001_mac_address_alloc");
			logger.debug("sp001_mac_address_alloc  p_mode=  " + p_mode + " p_trace_id=" + p_trace_id + " p_serial=" + p_serial
					+ " p_idnumber_id=" + p_idnumber_id + " p_alloc_qty=" + p_alloc_qty + " p_file_version="
					+ p_file_version + " p_mac_address=" + newMACAddress);
			System.out.println("10  " + p_mode + " p_trace_id=" + p_trace_id + " p_serial=" + p_serial
					+ " p_idnumber_id=" + p_idnumber_id + " p_alloc_qty=" + p_alloc_qty + " p_file_version="
					+ p_file_version + " p_mac_address=" + newMACAddress); // +"
																			// p_error="+"''"+"
																			// p_error="+"''");
																			// //+"
																			// p_mac_address="+p_mac_address);
			String[] paramList = { p_mode, p_trace_id, p_serial, p_idnumber_id, p_alloc_qty, p_file_version,
					newMACAddress };

			//System.out.println("sql= " + sql);
			storedProc = conn.prepareCall(sql);
			storedProc.setString(1, p_mode); // 0 = connect , 1 = disconnect
			storedProc.setString(2, p_trace_id); // 0 = connect , 1 = disconnect
			storedProc.setString(3, p_serial); // 0 = connect , 1 = disconnect
			storedProc.setString(4, p_idnumber_id); // 0 = connect , 1 =
													// disconnect
			storedProc.setString(5, p_alloc_qty); // 0 = connect , 1 =
													// disconnect
			storedProc.setString(6, p_file_version); // 0 = connect , 1 =
														// disconnect
			storedProc.setString(7, newMACAddress); // 0 = connect , 1 =
													// disconnect

			storedProc.registerOutParameter(8, Types.INTEGER);
			storedProc.registerOutParameter(9, Types.VARCHAR);
			storedProc.execute();
			

			String error = String.valueOf(storedProc.getString(8));
			String newMacAddress = String.valueOf(storedProc.getString(9));
			if (!error.equals("0")) {
				System.out.println("error= " + error);
				logger.debug("error= " + error);
			}
			jb = Json.createArrayBuilder();
			jpb = Json.createObjectBuilder();
			jpb.add("Error", error);
			jpb.add("New_MAC_Address", newMacAddress);
			jb.add(jpb);
			jsonResult.add("sp001_mac_address_alloc", jb);

			System.out.println(jb);
            
			//// return createJSONResponse( paramList, "inventory", sql,
			//// "sp001_mac_address_alloc");

		} catch (ConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if (conn != null)
				super.closeConnection(conn);
		}

		String json = jsonResult.build().toString();

		JsonParser jp = new JsonParser();
		JsonElement je = jp.parse(json);
		String prettyJsonString = gson.toJson(je);
		System.out.println("json= " + prettyJsonString);
		logger.debug(prettyJsonString);

		if (jb == null) {

			return Response.status(Response.Status.BAD_REQUEST).build();

		} else {

			return Response.ok(prettyJsonString).build();

		}
		
	}

	private Response createJSONResponse(String[] paramList, String dbScheme, String sql, String jsonHeader) {

		JsonObjectBuilder jpb = Json.createObjectBuilder();
		JsonArrayBuilder jb = Json.createArrayBuilder();
		JsonObjectBuilder jsonResult = Json.createObjectBuilder();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		Connection connPool = null;

		long longValue = 0;
		float floatValue = 0;
		String stringValue = null;
		int itemsCounter = 0;

		try {

			System.out.println("sql= " + sql);
			String datasource = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, dbScheme);
			connPool = super.getConnection(datasource);

			if (paramList.length == 1 && paramList[0].isEmpty()) {
				// System.out.println("paramList Is empty");
				// System.out.println(paramList[0]);
				paramList = null;
			}
			
			

	        if(connPool == null) {
	        	   
	        	   logger.debug("connPool= "+connPool+" createJSONResponse");
	        	   
	        	   return Response.status(Response.Status.BAD_REQUEST).build();
	           }
	           
			ResultSet rs = super.getQueryResult(connPool, sql, paramList);
			if (rs == null) {

				System.out.println("rs == null" + "  Conn=" + connPool + "  sql=" + sql);
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
			}

			ResultSetMetaData metaData = rs.getMetaData();

			int columns = metaData.getColumnCount();

			jb = Json.createArrayBuilder();
			while (rs.next()) {

				jpb = Json.createObjectBuilder();
				for (int i = 1; i <= columns; i++) { // 2 last columns are
														// rnum(ROWNUM) and
														// RESULT_COUNT so we
														// can ignore that
					if (metaData.getColumnLabel(i).toLowerCase().equals("rnum"))
						continue;
					if (metaData.getColumnLabel(i).toLowerCase().equals("result_count")) {
						itemsCounter = rs.getInt(i);
						continue;
					}
					if (metaData.getColumnTypeName(i).equals("NUMBER")) {
						longValue = rs.getLong(i);
						jpb.add(metaData.getColumnLabel(i).toLowerCase(), longValue);
					} else {
						stringValue = rs.getString(i);
						System.out.println("stringValue= " + stringValue + "  rs.getString(i)= " + rs.getString(i));
						if (stringValue == null)
							stringValue = "";
						jpb.add(metaData.getColumnLabel(i).toLowerCase(), stringValue);
					}
				}
				jb.add(jpb);

			}
			if (itemsCounter > 0)
				jsonResult.add("totalItems", itemsCounter);
			jsonResult.add(jsonHeader, jb);
		} catch (ConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		} finally {
			if (connPool != null)
				super.closeConnection(connPool);

		}
		String json = jsonResult.build().toString();

		JsonParser jp = new JsonParser();
		JsonElement je = jp.parse(json);
		String prettyJsonString = gson.toJson(je);
		System.out.println("json= " + prettyJsonString);
		logger.debug("createJSONResponse= " + prettyJsonString);
		if (jb == null) {

			return Response.status(Response.Status.BAD_REQUEST).build();

		} else {

			return Response.ok(prettyJsonString).build();

		}
	}

	@GET
	@Path("/Ping")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
	public Response getPing(@QueryParam("params") String params) {

		// System.out.println("getPing Build #3# 13 11 2017");
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		// System.out.println(dateFormat.format(cal.getTime()));
		String hostname = null;
		try {

			InetAddress ip = InetAddress.getLocalHost();
			hostname = ip.getHostName();

		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// String[] paramList = { params};
		JsonObjectBuilder jpb = Json.createObjectBuilder();
		JsonArrayBuilder jb = Json.createArrayBuilder();
		JsonObjectBuilder jsonResult = Json.createObjectBuilder();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		try {

			jpb = Json.createObjectBuilder();
			jpb.add("ping result", "ping + | date= " + dateFormat.format(cal.getTime()) + "  build=" + BUILD
					+ " ~~~ Server ~~~  = " + hostname);
			jb.add(jpb);
			jsonResult.add("Lists", jb);
		} finally {

		}

		String json = jsonResult.build().toString();

		JsonParser jp = new JsonParser();
		JsonElement je = jp.parse(json);
		String prettyJsonString = gson.toJson(je);
		logger.debug(prettyJsonString);
		if (jb == null) {

			return Response.status(Response.Status.BAD_REQUEST).build();

		} else {

			return Response.ok(prettyJsonString).build();

		}
	}

	// https://www.journaldev.com/1443/java-credit-card-validation-luhn-algorithm-java
	@POST
	@Path("/check_imei_valid")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
	public Response validateIMEINumber(@FormParam("imei") String imei) {

		String result = "false";
		// System.out.println("imei1 "+imei);
		int[] ints = new int[imei.length()];
		for (int i = 0; i < imei.length(); i++) {
			try {
				ints[i] = Integer.parseInt(imei.substring(i, i + 1));
			} catch (NumberFormatException nfe) {
				String output = "{\"result\":false}";
				return Response.ok(output).build();

			}
		}
		for (int i = ints.length - 2; i >= 0; i = i - 2) {
			int j = ints[i];
			j = j * 2;
			if (j > 9) {
				j = j % 10 + 1;
			}
			ints[i] = j;
		}
		int sum = 0;
		for (int i = 0; i < ints.length; i++) {
			sum += ints[i];
		}
		if (sum % 10 == 0) {

			// System.out.println(imei + " is a valid imei");
			logger.debug(imei + " is a valid imei");
			result = "true";

		} else {

			// System.out.println(imei + " is an invalid imei");
			logger.debug(imei + " is a valid imei");
			result = "false";
		}

		String output = "{\"result\":" + result + "}";
		return Response.ok(output).build();
	}

	private synchronized String getCurrMACAddress(Connection conn) {

		int MAC_ADDRESS = 1;

		ResultSet rs = null;

		String operationItem = "";

		String sql = ModelUtils.getResource(ORACLE_RESOURCES, "qry001.getcurrMAC.address");

		Object[] paramList = new Object[0];
		System.out.println("sql= " + sql);
		// Get the ResultSet
		rs = super.getQueryResult(conn, sql, paramList);

		try {
			while (rs.next()) {

				operationItem = rs.getString(MAC_ADDRESS);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// }

		return operationItem;
	}

	private String CalcNewMACAddress(String CurrMACAddress, int CurrAllocQty) {

		String resultA = null;
		String resultB = null;
		resultA = CurrMACAddress.substring(0, 5); // Copy(CurrMACAddress,1,5)
													// 0020D of 0020D2EDA4D0

		int temp = Integer.valueOf(String.valueOf(CurrMACAddress.substring(5, 5 + 7)), 16); // 49128656

		resultB = Integer.toHexString(temp - CurrAllocQty); // 2eda4c6
		resultB = resultA + resultB;
		logger.debug("");
		logger.debug("      last 7 CurrMACAddress in dec= " + temp);
		logger.debug("      CurrMACAddress B4 Alloc  = " + CurrMACAddress);
		logger.debug("      Requested Qty    = " + CurrAllocQty);
		logger.debug("      New Dec:" + temp + " - " + CurrAllocQty + " = " + (temp - CurrAllocQty));
		logger.debug("		New MAC address in DB = " + resultB.toUpperCase()); // 0020D2EDA4C6
		
		logger.debug("");

		return resultB;
	}

	public static Connection getOracleJDBCConnection(String url, String userid, String password) {

		Connection conn = null;

		try {
			conn = DriverManager.getConnection(url, userid, password);
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
		}

		return conn;
	}

	@POST
	@Path("/getConfirmationMessage")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
	public Response getConfirmationMessage(@FormParam("idnumber") String idnumber, @FormParam("mac1") String mac1,
			@FormParam("mac2") String mac2, @FormParam("imei1") String imei1, @FormParam("imei2") String imei2) {

		// public void getConfirmationMessage(String idnumber, String mac1,
		// String mac2, String imei1,String imei2){

		String[] paramList1 = new String[1];
		String[] paramList2 = new String[2];
		StringBuffer sb = new StringBuffer();
		ArrayList<String> resultMessage = new ArrayList<>();
		int indexMessage = 1;
		JsonObjectBuilder jpb = Json.createObjectBuilder();
		JsonArrayBuilder jb = Json.createArrayBuilder();
		JsonObjectBuilder jsonResult = Json.createObjectBuilder();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		jb = Json.createArrayBuilder();
		// jpb = Json.createObjectBuilder();
		// jsonResult.add("totalItems", itemsCounter);
		ArrayList<String> newMACS = new ArrayList<String>();
		ArrayList<String> newIMEIs = new ArrayList<String>();
		if (mac1 != null && mac1.length() > 0)
			newMACS.add(mac1.trim());
		if (mac2 != null && mac2.length() > 0)
			newMACS.add(mac2.trim());
		if (imei1 != null && imei1.length() > 0)
			newIMEIs.add(imei1.trim());
		if (imei2 != null && imei2.length() > 0)
			newIMEIs.add(imei2.trim());
		String datasource = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "inventory");
		Connection connPool = null;
		try {

			connPool = super.getConnection(datasource);

		} catch (ConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 if(connPool == null) {
      	   
      	   logger.debug("connPool= "+connPool+" getConfirmationMessage");
      	   
      	   return Response.status(Response.Status.BAD_REQUEST).build();
         }

		try {
			System.out.println("mac1= " + mac1 + " mac2= " + mac2 + " imei1= " + imei1 + " imei2= " + imei2);
			/** 1. Check if IMEI1 is connected to ID Check Overwrite IMEI */

			resultMessage = CheckIfIDisConnectedToIMEI(connPool, idnumber, mac1, paramList2, newIMEIs);

			/**
			 * 2. Check if new IMEI is already in DB connected to some Other ID
			 */

			resultMessage.addAll(CheckIfIMEIisUsed(connPool, idnumber, imei1, paramList1));

			/**
			 * 3. Check if IMEI2 is connected to ID Check Overwrite IMEI2 in DB
			 */

			resultMessage.addAll(CheckIfIDisConnectedToIMEI2(connPool, idnumber, mac1, paramList2, newIMEIs));

			/**
			 * 4. Check if new IMEI2 is already in DB connected to some Other ID
			 */
			resultMessage.addAll(CheckIfIMEI2isUsed(connPool, idnumber, imei2, paramList1));

			/**
			 * 5. MAC1 already used ( with many ID's ) Delete All Id numbers
			 * with the same MAC. Replacing with new MAC and ID
			 * q001_mac_extant_chack
			 */
			resultMessage.addAll(CheckIfMAC1isConnectedToOtherID(connPool, idnumber, mac1, paramList1));

			/**
			 * 5.2 MAC2 already used ( with many ID's ) Delete All Id numbers
			 * with the same MAC. Replacing with new MAC and ID
			 * q001_mac_extant_chack
			 */
			resultMessage.addAll(CheckIfMAC2isConnectedToOtherID(connPool, idnumber, mac2, paramList1));

			/**
			 * 6. Id Number is already used. //Delete All Id numbers with the
			 * same MAC. //Replacing with new MAC and ID
			 */
			resultMessage.addAll(CheckIfIDisConnectedToOtherMAC(connPool, idnumber, newMACS, paramList1));
	

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (connPool != null)
				super.closeConnection(connPool);

		}
		if (resultMessage != null && resultMessage.size() > 0 ){
			ListIterator<String> li = resultMessage.listIterator();
			while(li.hasNext())				
				sb.append("*" + li.next() + "@@");
			
		}
		jsonResult.add("ConfirmationMessage", sb.toString());
		jb.add(jsonResult);
		jpb.add("msg", jb);
		// String json = jsonResult.build().toString();
		String json = jpb.build().toString();

		JsonParser jp = new JsonParser();
		JsonElement je = jp.parse(json);
		String prettyJsonString = gson.toJson(je);
		//System.out.println("json= " + prettyJsonString);
		logger.debug(prettyJsonString);
		// String output = "{\"result\":"+result+"}";
		return Response.ok(prettyJsonString).build();

	}
	

	/**
	 * 1. Check if IMEI1 is connected to ID Check Overwrite IMEI
	 * 
	 * @throws SQLException
	 */

	private ArrayList<String> CheckIfIDisConnectedToIMEI(Connection connPool, String idnumber, String mac1,
			String[] paramList2, ArrayList newIMEIs) throws SQLException {

		String sql_q004_imei_check = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "q004_imei_check");
		ArrayList<String> resultMessage = new ArrayList<String>();
		paramList2[0] = mac1.trim();
		paramList2[1] = idnumber.trim(); // , imei };
		ResultSet rs = makeQuery(paramList2, sql_q004_imei_check, connPool);

		while (rs.next()) {
			// && imeiDB != null && rs.getString("IMEI").length() > 0){ // id is
			// connected to IMEI
			String imeiDB = rs.getString("IMEI");
			if (newIMEIs.contains(imeiDB) == false) {
				System.out.println("Current ID " + idnumber + " is connected to IMEI1 = " + imeiDB);
				resultMessage.add(" ID " + idnumber + " already conn to IMEI1 " + imeiDB);
			}
		}
		return resultMessage;
	}

	/**
	 * 2. Check if new IMEI is already in DB connected to some Other ID
	 * 
	 * @throws SQLException
	 */
	private ArrayList<String> CheckIfIMEIisUsed(Connection connPool, String idnumber, String imei1, String[] paramList1)
			throws SQLException {

		ArrayList<String> resultMessage = new ArrayList<String>();
        ArrayList<String> idNumberDBResutlList = new ArrayList<String>();
		if (imei1 != null && imei1.length() > 0) {
			String sql_q005_imei_check = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "q005_imei_check");
			paramList1[0] = imei1.trim(); // , imei };
			ResultSet rs = makeQuery(paramList1, sql_q005_imei_check, connPool);

			while (rs.next()) {
				String idNumberDB = rs.getString("ID_NUMBER");
				
				if (idNumberDB != null && idNumberDB.length() > 0 && idNumberDB.equals(idnumber) == false && !idNumberDBResutlList.contains(idNumberDB)) {
					idNumberDBResutlList.add(idNumberDB);
					System.out.println(" Current IMEI1 " + imei1 + " is connected to other ID  " + idNumberDB);
					resultMessage.add(" IMEI1 " + imei1 + " already conn to ID " + idNumberDB);

				}
			}
		}
		return resultMessage;
	}

	/**
	 * 3. Check if IMEI2 is connected to ID Check Overwrite IMEI2 in DB
	 * 
	 * @throws SQLException
	 */
	private ArrayList<String> CheckIfIDisConnectedToIMEI2(Connection connPool, String idnumber, String mac1,
			String[] paramList2, ArrayList newIMEIs) throws SQLException {

		ArrayList<String> resultMessage = new ArrayList<String>();
		
		if (mac1 != null && idnumber != null && mac1.length() > 0 && idnumber.length() > 0) {
			String q0041_imei_check_imei2 = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "q0041_imei_check_imei2");
			paramList2[0] = mac1.trim();
			paramList2[1] = idnumber.trim();
			ResultSet rs = makeQuery(paramList2, q0041_imei_check_imei2, connPool);
			
			while (rs.next()) {
				String imeiDB = rs.getString("IMEI2");
				
				if (newIMEIs.contains(imeiDB) == false) {	
					System.out.println("Current ID is connected to IMEI2 " + imeiDB + " idnumber= " + idnumber);
					resultMessage.add(" ID " +idnumber+" already conn to IMEI2 " + imeiDB );
					// sb.append(System.getProperty("line.separator"));

				}
			}
		}
		return resultMessage;
	}

	/**
	 * 4. Check if new IMEI2 is already in DB connected to some Other ID
	 * 
	 * @throws SQLException
	 */
	private ArrayList<String> CheckIfIMEI2isUsed(Connection connPool, String idnumber, String imei2,
			String[] paramList1) throws SQLException {
		ArrayList<String> resultMessage = new ArrayList<String>();
		ArrayList<String> idNumberDBResutlList = new ArrayList<String>();
		
		if (imei2 != null && imei2.length() > 0) {
			String q0051_imei_check_imei2 = ModelUtils.getResource(IConsts.ORACLE_RESOURCES, "q0051_imei_check_imei2");
			paramList1[0] = imei2.trim();

			ResultSet rs = makeQuery(paramList1, q0051_imei_check_imei2, connPool);

			// if (rs != null ){
			while (rs.next()) {
				String idNumberDB = rs.getString("ID_NUMBER");
				if (idNumberDB != null && idNumberDB.length() > 0 && idNumberDB.equals(idnumber) == false && !idNumberDBResutlList.contains(idNumberDB)) {
					idNumberDBResutlList.add(idNumberDB);
					System.out
							.println("Current IMEI2 " + imei2 + " is connected to other ID idNumberDB= " + idNumberDB);
					resultMessage
							.add(" IMEI2 " + imei2 + " already conn to ID " + idNumberDB);
					// sb.append(System.getProperty("line.separator"));

				}
			}
		}
		return resultMessage;
	}

	/**
	 * 5. MAC1 already used ( with many ID's ) Delete All Id numbers with the
	 * same MAC. Replacing with new MAC and ID q001_mac_extant_chack
	 * 
	 * @throws SQLException
	 */
	private ArrayList<String> CheckIfMAC1isConnectedToOtherID(Connection connPool, String idnumber, String mac1,
			String[] paramList1) throws SQLException {
		ArrayList<String> resultMessage = new ArrayList<String>();
		if (mac1 != null && mac1.length() > 0) {
			String qry_q001_mac_extant_chack = ModelUtils.getResource(IConsts.ORACLE_RESOURCES,
					"qry.q001.mac.extant.chack");
			paramList1[0] = mac1.trim();

			ResultSet rs = makeQuery(paramList1, qry_q001_mac_extant_chack, connPool);

			// if (rs != null ){
			while (rs.next()) {
				String idNumberDB = rs.getString("ID_NUMBER");
				if (idNumberDB != null && idNumberDB.length() > 0 && idNumberDB.equals(idnumber) == false) {
					System.out.println("Current MAC " + mac1 + " is connected to other ID = " + idNumberDB);
					resultMessage.add(" MAC " + mac1 + " already conn to ID " + idNumberDB);
					// sb.append(System.getProperty("line.separator"));

				}
			}
		}
		return resultMessage;
	}

	/**
	 * 5.2 MAC2 already used ( with many ID's ) Delete All Id numbers with the
	 * same MAC. Replacing with new MAC and ID q001_mac_extant_chack
	 * 
	 * @throws SQLException
	 */
	private ArrayList<String> CheckIfMAC2isConnectedToOtherID(Connection connPool, String idnumber, String mac2,
			String[] paramList1) throws SQLException {
		ArrayList<String> resultMessage = new ArrayList<String>();

		if (mac2 != null && mac2.length() > 1) {
			String qry_q001_mac_extant_chack = ModelUtils.getResource(IConsts.ORACLE_RESOURCES,
					"qry.q001.mac.extant.chack");
			paramList1[0] = mac2.trim();

			ResultSet rs = makeQuery(paramList1, qry_q001_mac_extant_chack, connPool);

			// if (rs != null ){
			while (rs.next()) {
				String idNumberDB = rs.getString("ID_NUMBER");
				if (idNumberDB != null && idNumberDB.length() > 0 && idNumberDB.equals(idnumber) == false) {
					System.out.println("Current MAC " + mac2 + " is connected to other ID = " + idNumberDB);
					resultMessage.add(" MAC " + mac2 + " already conn to ID " + idNumberDB);
					// sb.append(System.getProperty("line.separator"));

				}
			}
		}
		return resultMessage;
	}

	/**
	 * 6. Id Number is already used. //Delete All Id numbers with the same MAC.
	 * //Replacing with new MAC and ID
	 * 
	 * @throws SQLException
	 */
	private ArrayList<String> CheckIfIDisConnectedToOtherMAC(Connection connPool, String idnumber, ArrayList newMACS,
			String[] paramList1) throws SQLException {
		ArrayList<String> resultMessage = new ArrayList<String>();

		if (idnumber != null && idnumber.length() > 0) {
			String q003_idnumber_extant_check = ModelUtils.getResource(IConsts.ORACLE_RESOURCES,
					"q003_idnumber_extant_check");
			paramList1[0] = idnumber.trim();

			ResultSet rs = makeQuery(paramList1, q003_idnumber_extant_check, connPool);

			while (rs.next()) {
				String macDB = rs.getString("MAC");
				if (newMACS.contains(macDB) == false) {
					System.out.println("Current ID " + idnumber + " is connected to other MAC =" + macDB);
					resultMessage.add(" ID " + idnumber + " already conn to MAC " + macDB);
					// sb.append(System.getProperty("line.separator"));

				}
			}
		}
		return resultMessage;
	}

	private ResultSet makeQuery(String[] params, String sql, Connection connPool) {

		if (params.length == 1 && params[0].isEmpty()) {
			params = null;
		}

		ResultSet rs = super.getQueryResult(connPool, sql, params);
		if (rs == null) {

			System.out.println("rs == null" + "  Conn=" + connPool + "  sql=" + sql);
			return null;
		}

		return rs;

	}
}

/*
 * Created on Oct 1, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.rad.macreg.rest.errors;

import com.rad.macreg.rest.model.ModelUtils;



/**
 * @author eyal_r
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ModelException extends Exception {

	private static final long serialVersionUID = 1L;
	/**
	 * Main Model Exception
	 */
	
	private static final String MESSAGES_RESOURCES = "/WEB-INF/messages-resources";
	
	public ModelException() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public ModelException(String message) {
		super(ModelUtils.getResource(MESSAGES_RESOURCES, message));
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ModelException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public ModelException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}

/*
 * Created on Oct 9, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.rad.macreg.rest.model;

import java.sql.Connection;
import java.sql.ResultSet;

import com.rad.macreg.rest.errors.ConnectionException;



/**
 * @author eyal_r
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface IUtilsDAO extends IConsts {
	public Connection getConnection() throws ConnectionException;
	//public Connection getConnection(String scheme, String password) throws ConnectionException;
	public void closeConnection(Connection conn);
	public ResultSet getQueryResult(Connection conn, String sql, Object[] paramList);
	public boolean updateQuery(Connection conn, String sql, Object[] paramList);
}

/*
 * Created on Oct 1, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.rad.macreg.rest.model;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ResourceBundle;

/**
 * @author eyal_r
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public final class ModelUtils {

	/**
	 * Model Utils
	 */
	
	private static ResourceBundle resources;
	private static String RESOURCES; // resources file name
	
	private static synchronized ResourceBundle getResources(String _RESOURCES) {
		if (RESOURCES.equals(_RESOURCES)) {
			if (resources == null) {
				resources = ResourceBundle.getBundle(_RESOURCES);
			}
		} else {
			RESOURCES = _RESOURCES;
			
			resources = null;
			resources = ResourceBundle.getBundle(_RESOURCES);
		}
        return resources;
    }
	
	
    public static String getResource(String _RESOURCES, String key) {
    	if (RESOURCES == null) {
    		RESOURCES = _RESOURCES;
    	}
    	return getResources(_RESOURCES).getString(key);
    }    
    
    public static void copy(Object source, Object dest) {
        try {
            Class sourceClass = source.getClass();
            Class destClass = dest.getClass();
            BeanInfo info = Introspector.getBeanInfo(sourceClass);
            PropertyDescriptor props[]
                = info.getPropertyDescriptors();
            Object noParams[] = new Object[0];
            Object oneParam[] = new Object[1];
            for (int i = 0; i < props.length; i++) {
                Method getter = props[i].getReadMethod();
                if (getter == null)
                    continue;
                Object value = getter.invoke(source, noParams);
                Method setter = props[i].getWriteMethod();
                if (setter != null && sourceClass != destClass)
                    try {
                        setter = destClass.getMethod(
                            setter.getName(),
                            setter.getParameterTypes());
                    } catch (NoSuchMethodException x) {
                        setter = null;
                    }
                if (setter != null) {
                    oneParam[0] = value;
                    setter.invoke(dest, oneParam);
                }
            }
        } catch (IntrospectionException x) {
            throw new InternalError(x.getMessage());
        } catch (IllegalAccessException x) {
            throw new InternalError(x.getMessage());
        } catch (IllegalArgumentException x) {
            throw new InternalError(x.getMessage());
        } catch (SecurityException x) {
            throw new InternalError(x.getMessage());
        } catch (InvocationTargetException x) {
            throw new InternalError(x.getTargetException().getMessage());
        }
    }
}

/*
 * Created on Oct 9, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.rad.macreg.rest.dao.implementation;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.NamingException;

import com.rad.macreg.rest.dao.resource.ConnectionPool;
import com.rad.macreg.rest.dao.resource.OracleQueryPool;
import com.rad.macreg.rest.errors.ConnectionException;
import com.rad.macreg.rest.model.IUtilsDAO;
import com.rad.macreg.rest.model.ModelUtils;


/**
 * @author eyal_r
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class UtilsDAO implements IUtilsDAO {

	/**
	 * Utils DAO: Oracle implementation
	 */
	
	protected Connection conn = null;	
	private ConnectionPool oracle = new ConnectionPool();
	private OracleQueryPool query = new OracleQueryPool();
	
	public UtilsDAO() {				
	}

	/* (non-Javadoc)
	 * @see com.rad.sales.totalsytd.model.dao.logic.IUtilsDAO#closeConnection(java.lang.Object)
	 */
	public void closeConnection(Connection conn) {
		try {			
			oracle.closeConnection(conn);
		} catch (SQLException e) {			
			e.printStackTrace();
		}
	}
	
	/* (non-Javadoc)
	 * @see com.rad.sales.totalsytd.model.dao.logic.IUtilsDAO#getQueryResult(java.lang.Object, java.lang.String, java.lang.Object)
	 */
	public ResultSet getQueryResult(Connection conn, String sql, Object[] paramList) {		
		try {			
			return query.getQueryResult(conn, sql, paramList);
		} catch (SQLException e) {			
			e.printStackTrace();
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.rad.sales.totalsytd.model.dao.logic.IUtilsDAO#getQueryResult(java.lang.Object, java.lang.String, java.lang.Object)
	 */
	public boolean updateQuery(Connection conn, String sql, Object[] paramList) {		
		boolean result = false;
		try {			
			 result = query.updateQuery(conn, sql, paramList);
		} catch (SQLException e) {			
			e.printStackTrace();
		}
		return result;
	}
	/* (non-Javadoc)
	 * @see com.rad.sales.totalsytd.model.dao.logic.IUtilsDAO#getConnection()
	 */
	public Connection getConnection() throws ConnectionException {
		try {
//System.out.println("getConnection new 2");			
			this.conn = oracle.getConnection(ModelUtils.getResource(ORACLE_RESOURCES, DATASOURCE_WORKFLOW));
			//String url = ModelUtils.getResource(ORACLE_RESOURCES, URL);
			//String user = ModelUtils.getResource(ORACLE_RESOURCES, USER);
			//String pass = ModelUtils.getResource(ORACLE_RESOURCES, PASS);
			//this.conn = oracle.getOracleJDBCConnection(url,user,pass);
//System.out.println("getConnection 2"+conn);			
		}catch (NamingException e1) {				
			e1.printStackTrace();
		}catch (SQLException e) {				
			e.printStackTrace();
		}
//System.out.println("conn= "+conn);		
		return conn;
	}
	

	
	/* (non-Javadoc)
	 * @see com.rad.sales.totalsytd.model.dao.logic.IUtilsDAO#getConnection()
	 */
	public Connection getConnection(String DS_Name) throws ConnectionException {
			try {
	//System.out.println("getConnection for "+DS_Name);			
				this.conn = oracle.getConnection(DS_Name);
			} catch (NamingException e) {					
				e.printStackTrace();
			} catch (SQLException e) {					
				e.printStackTrace();
			}
	
		return conn;
	}
}

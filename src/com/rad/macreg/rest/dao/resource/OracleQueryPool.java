/*
 * Created on Jan 14, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.rad.macreg.rest.dao.resource;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

//import org.apache.log4j.Logger;

/**
 * @author Michal_h
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

public class OracleQueryPool {

	//static Logger logger = Logger.getLogger(OracleQueryPool.class);

	private PreparedStatement stmt = null;

	private ResultSet rs = null;

	public OracleQueryPool() {
	}

	public ResultSet getQueryResult(Connection conn, String sql,
			Object[] argList) throws SQLException {
		//Get prepare statement	
		System.out.println(" getQueryResult conn= "+conn);
		stmt = conn.prepareStatement(sql);
		
		//Set query parameters
		if (argList != null && argList.length > 0) {
			for (int i = 0; i < argList.length; i++) {
				
				//System.out.println("getQueryResult = "+argList[i]+" .length. "+argList.length);
				stmt.setObject(i + 1, argList[i]);
			}
		}

		//Execute query	
		rs = (ResultSet) stmt.executeQuery();
		return rs;
	}

	public boolean updateQuery(Connection conn, String sql, Object[] argList)
			throws SQLException {
		//Get prepare statement			
		//boolean result = false;
		stmt = conn.prepareStatement(sql);
		//logger.debug("conn");
		//Set query parameters
		if (argList != null) {
			for (int i = 0; i < argList.length; i++) {
				stmt.setObject(i + 1, argList[i]);
			}
		}

		//Execute query	
		stmt.executeUpdate();
		//conn.commit();
		//logger.debug("finished updateQuery succesfuly");
		return true;
	}
}

/*
 * Created on Feb 16, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.rad.macreg.rest.dao.resource;

import java.sql.SQLException;

/**
 * @author eyal_r
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class OracleMessages {

	/**
	 * Oracle Messages: SQLException Object and Information messages
	 */

	private SQLException exception = null;
	
	public OracleMessages() {
		super();		
	}
	
	public String getErrorMessage(Exception ex) {
		exception = (SQLException) ex;
		if (exception.getErrorCode() == 0) {
			return ex.getMessage();
		} else {
			return (exception.getErrorCode() + " " + exception.getMessage());
		}
	}
}

/*
 * Created on Jan 11, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.rad.macreg.rest.dao.resource;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.rad.macreg.rest.internal.model.MacRegREST;

import oracle.jdbc.driver.*;
import oracle.jdbc.*;


/**
 * @author Michal_h
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ConnectionPool {
	
	private static  DataSource inventory;
	private static  DataSource inventory_dev;
	private static  Connection localConn;
	
		
	
							
							
	public ConnectionPool(){
		
				
	}
	
		
	//connection by default shceme on the server
	public Connection getConnection(String datasourceName) throws NamingException, SQLException {

		
		System.out.println("datasourceName= "+datasourceName);
		if  ( checkForLocalIPAddress() && MacRegREST.appDebug  ){
			//String url = "jdbc:oracle:thin:@radb1.ad.rad.co.il:1521/prd1";//192.114.24.54
			String url = "jdbc:oracle:thin:@192.114.24.54:1521/prd1";
			//String url = "jdbc:oracle:thin:@192.168.24.24:1521/e0905dev";
			System.out.println("checkForLocalIPAddress= True 1");
			
			return getOracleJDBCConnection( url,"inventory","stock"); //getConnectionLocal(datasourceName);
		}
	    switch (datasourceName) {
	    
		case "java:comp/env/jdbc/radb1/inventoryDev":
			if (inventory_dev == null){
				inventory_dev = setInitContext("java:comp/env/jdbc/radb1/inventoryDev");
				System.out.println("Initialized inventory_dev  "); 
			}
			return inventory_dev.getConnection();
			
		case "java:comp/env/jdbc/radb1/inventory":
			if (inventory == null){
				inventory =  setInitContext("java:comp/env/jdbc/radb1/inventory");
				System.out.println("Initialized inventory  ");
			}
			return inventory.getConnection();
			
		
		default:
			break;
		}
	    System.out.println("getConnection  datasourceName= "+datasourceName+" retrun null !!");
		return null;
	}
	
	//connection by default shceme on the server
		public DataSource setInitContext(String datasourceName) throws NamingException, SQLException {

			// get the initial context
							
			InitialContext ic = new InitialContext();
		
			DataSource datasource = (DataSource) ic.lookup(datasourceName);
		
			return datasource;
		}
		
		
	public void closeConnection(Connection conn) throws SQLException{
		conn.close();
	}
	
	
	
	public Connection getConnectionLocal(String datasourceName){
		
		 Context ctx = null;
		 Connection conn = null;
		    try {
		      Properties prop = new Properties();
		      prop.setProperty(Context.INITIAL_CONTEXT_FACTORY,
		          "com.sun.jndi.fscontext.RefFSContextFactory");
		     
		      ctx = new InitialContext(prop);
		    } catch (NamingException ne) {
		      System.err.println(ne.getMessage());
		    }

		    DataSource datasource;
			try {
				
				datasource = (DataSource) ctx.lookup(datasourceName);
			
		  	    conn = datasource.getConnection();
		  		
		  		
		   // DataSource ds = (DataSource) ctx.lookup(datasourceName);
		   // Connection conn = ds.getConnection();
		    
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		   /* Statement stmt = conn.createStatement();
		    ResultSet rset = stmt
		        .executeQuery("select 'Hello Thin driver data source tester '||"
		            + "initcap(USER)||'!' result from dual");
		    if (rset.next())
		      System.out.println(rset.getString(1));
		    rset.close();
		    stmt.close();
		    conn.close();*/ catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return conn;
		 
	}
	
	public static Connection getOracleJDBCConnection(String url,String userid,String password){
		
		 //Registering the driver
        try {
        	
			DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
			
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//Connection conn=null;
		if (localConn != null){
			return localConn;
		}
		/*try {
			Class.forName("oracle.jdbc.OracleJdbcDriver");	
		} catch(java.lang.ClassNotFoundException e) {
			System.err.print("ClassNotFoundException:");
			System.err.println(e.getMessage());
		}*/

		try {
			localConn = DriverManager.getConnection(url, userid, password);
		} catch(SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
		}

		return localConn;
	}
	
	
	
	boolean  checkForLocalIPAddress() {
		
	        InetAddress inetAddress;
			try {
				inetAddress = InetAddress.getLocalHost();
			
	       System.out.println("IP Address:- " + inetAddress.getHostAddress());
	        System.out.println("Host Name:- " + inetAddress.getHostName());
	        
	        return inetAddress.getHostAddress().equals("172.17.130.144");  // Check for my own IP
	        
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
	   
	}
}

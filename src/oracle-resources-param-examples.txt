	   Procedures:
1.	RAD_JER.RT_HISTORY_MAINT
                      ParamByName('p_MODE').AsFloat :=2;
                      ParamByName('p_REEL_ID').AsFloat:= 816052;
                      ParamByName('p_FROM_ST_ID').AsFloat:=40;
                      ParamByName('p_TO_ST_ID').AsFloat:=99;
                      ParamByName('p_REPLACED_WITH_ID').AsFloat:= 816052;
                      ParamByName('p_KIT_NUMBER').AsString:= '24057430';
                      ParamByName('p_FEEDLOC').AsString:= 'F64';
                      ParamByName('p_USER_NAME').AsString:= 'NIR MIZRAHI';

2.	RAD_JER.RT_MAINT
        ParamByName('p_mode').AsInteger:= 0;
         ParamByName('p_reel_id').AsFloat:= 971656;
         ParamByName('p_pri_qty').AsFloat:= 10; 
         ParamByName('p_cur_qty').AsFloat:= 99999;
         ParamByName('p_emp_name').AsString:= 'NIR MIZRAHI';
         ParamByName('P_QTY_CHECKED').AsInteger := 0;
         ParamByName('P_RESULT').AsFloat:=0; // Dummy
         ParamByName('P_ERROR').AsString:='';

        ParamByName('P_MODE').AsInteger:= 1;
        ParamByName('P_ID').AsFloat:= 971861;
        ParamByName('p_cur_qty').AsFloat:= 99999;
        ParamByName('P_STATION_ID').AsInteger := 40;
        ParamByName('P_TRANS_DATE').AsDateTime:= Now;
        ParamByName('P_EMP_NAME').AsString:= 'ISRAEL ALEX GALPERN' ;
        ParamByName('P_QTY_CHECKED').AsInteger := 0;
        
        ParamByName('P_MODE').AsInteger:= 10;
        ParamByName('P_REEL_ID').AsFloat:= 971656;
        ParamByName('P_STATION_ID').AsInteger := 45;
        ParamByName('P_TRANS_DATE').AsDateTime:= Now;
        ParamByName('P_EMP_NAME').AsString:= 'YOSSI KAHANI';
        ParamByName('P_REMARKS').AsString:= 'Pulled from: 24056034';

3.	RAD_JER.RT_COUNTER_MAINT
     ParamByName('P_MODE').AsInteger:=0;
     ParamByName('P_REEL_ID').AsFloat:= 983652;
     ParamByName('P_KITTING_FILE').AsString:= �28058442�;
     ParamByName('P_FEEDER_LOC').AsString:= �FL08�;

4.	INVENTORY.STOCK_TRACE_2_PO_MAINT
   ParamByName('pMode').AsFloat := 0 ;
    ParamByName('pSTOCK_TRACE_ID').AsFloat := 828363 ;
    ParamByName('pPO_NUMBER').AsFloat := 5612493;
    ParamByName('pASSEMBLY_PDN').AsFloat := 0;

5.	RAD_JER.J_PROD_SHOW_ST_UPD
   ParamByName('P_ID').AsFloat:= 183886;
   ParamByName('P_STATION_ID').AsInteger:= 5;
   ParamByName('P_PS').AsString:='';
   ParamByName('P_CS').AsString:='';
   ParamByName('P_LAST_UPDATE_AT').AsDateTime:=Now;

6.	RAD_JER.STOCK_TRACE_VERIFY
           ParamByName('P_CATALOG').AsString := �R-1005F30R1DNT� ;
  ParamByName('P_PO_NUMBER').AsFloat:= 5612493;
  ParamByName('P_STOCK_TRACE_ID').AsFloat := 813241;
   ParamByName('P_RESULT_NESESSARY').AsInteger :=0;
  ParamByName('P_RESULT_EXIST').AsInteger :=0;

7.	RAD_JER.PON_KIT_ITEMS_MAINT
      ParamByName('p_mode').AsInteger:= 3;
      ParamByName('p_id').AsFloat:= 1880968;
      ParamByName('p_carriage_num').AsInteger:= 77;

    ParamByName('P_MODE').AsInteger:= 0;
     ParamByName('P_PONK_HEAD_ID').AsFloat:= 239019;
     ParamByName('P_ADDR').AsString:= �W-6-S�;
     ParamByName('P_CATALOG').AsString:= �IC-NC7SZ126�;
     ParamByName('P_FEED_LOC').AsString:= �FR22�;
     ParamByName('P_FEED_TYPE').AsString:= �F3-8�;
     ParamByName('P_QTY').AsInteger:= 300;
     ParamByName('P_TAPE').AsString:= �F_EMBOSS�;
     ParamByName('P_NOTES').Clear;
     ParamByName('P_IMG_CODE').AsString:= �SOT-145�;                                    

8.	RAD_JER.REEL_REPLACEMENT_MAINT
   ParamByName('P_CATALOG').AsString:= �R-1608J000DNT�;
    ParamByName('P_REEL_ID_UNLOAD').AsString  :=  �L787561�;
    ParamByName('P_REEL_ID_LOADED').AsString  :=  �L981815�;
    ParamByName('P_LINE').AsString  :=  �LINE-1�;
    ParamByName('P_FEEDER_LOC').AsString  :=  �FR28�;
            
9.	RAD_JER.J_NEXT_INS
          ParamByName('P_ADDRESS').AsString := �Z2-7-G�;
          ParamByName('P_MKT').AsString := �HK-2-G�;
          ParamByName('P_FEEDLOC').AsString := �FL28�;
          ParamByName('P_FEEDTYPE').AsString := �F3-12�;
          ParamByName('P_QTY').AsString := 400;
          ParamByName('P_TAPE').AsString := �F_EMBOSS�;
          ParamByName('P_NOTES').AsString := '';
          ParamByName('P_KIYUT').AsString := �25059482�;
          ParamByName('P_KIYUTFOR').AsString := ' '; //space
          ParamByName('P_USED').AsString := ' '; //space
          ParamByName('P_PRINTED').AsString := ' '; //space
          ParamByName('P_DEMO').AsString := ' '; //space
          ParamByName('P_REEL_ID').AsFloat:= 990709;
          ParamByName('P_STATION_ID').AsInteger:= 45; 
          ParamByName('P_LINE').Clear;

10.	RAD_JER.PON_KIT_HEAD_MAIN
ParamByName('P_MODE').AsInteger:=3; 
ParamByName('P_CARRIAGE_NUM').AsInteger:=55;
ParamByName('P_KIT_NUMBER').AsString:= �28058442�;

ParamByName('P_MODE').AsInteger:=4; // kit_remarks
ParamByName('P_KIT_REMARKS').AsString:= �TEST 123�;
ParamByName('P_KIT_NUMBER').AsString:= �28058442�;

11.	RAD_JER.RT_MAIN_ADDTIONAL
   ParamByName('P_MODE').AsInteger := 11;
   ParamByName('P_REEL_ID').AsInteger:= 715125;
   ParamByName('P_VISUAL').AsInteger:=1;

Functions:
1.	RAD_JER.GET_MULTI_CARD_NUM
     ParamByName('p_pcb').AsString:= �RICI-16E1.REV2.0I�;

2.	PDM.GET_PDN
     ParamByName('pCATALOG').AsString  := 'ACE52-SYS.REV0.0I' ;


	                